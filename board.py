import random
from osgeo import ogr


class PlayerAction:

    def __init__(self, name, emitter, board_service):
        self.executed = False
        self.payload  = None
        self.name = name
        self.emitter = emitter
        self.board_service = board_service

    def set_payload(self, payload):
        self.payload = payload

    def emit(self):
        self.board_service.emit(self, self.emitter)

    def set_executed(self):
        self.executed = True


class PlayerActionMove(PlayerAction):

    NAME = "ACTION_CHANGE_POSITION"

    def __init__(self, emitter, board_service):
        super().__init__(
            PlayerActionMove.NAME,
            emitter,
            board_service
        )

    def run(self):
        target = self.payload["target"]
        target.set_position(self.payload["x"], self.payload["y"])


class BoardService:

    POSITIONS = [
        [0, 0],
        [1, 0],
        [2, 0],
        [3, 0],
        [0, 1],
        [1, 1],
        [2, 1],
        [3, 1],
        [0, 2],
        [1, 2],
        [2, 2],
        [3, 2],
        [0, 3],
        [1, 3],
        [2, 3],
        [3, 3],
        [0, 4],
        [1, 4],
        [2, 4],
        [3, 4]
    ]

    def __init__(self):
        self.action_requests = { k+1 : [] for k in range(90) }
        self.action_approved = { k+1 : [] for k in range(90) }
        self.players = list()

    def emit(self, action, emitter):
        if action.name == BoardPlayer.ACTION_CHANGE_POSITION and \
                [action.payload["x"], action.payload["y"]] in BoardService.POSITIONS:
            action.run()
            action.set_executed()

    def run(self):
        for second in self.action_approved.keys():
            print("SECONDO: {}".format(second))
            for player in self.players:
                print(player.current_point)
                player.run_turn()

    def add_player(self, player):
        self.players.append(player)


class BoardPlayer:

    ACTION_CHANGE_POSITION = "ACTION_CHANGE_POSITION"

    def __init__(self, board_service, x, y):
        self.board_service = board_service
        self.current_point = None
        self.position_history = list()
        self.actions = list()

        self.set_position(x, y)

    def set_position(self, x, y):
        self.current_point = ogr.Geometry(ogr.wkbPoint)
        self.current_point.AddPoint(x, y)
        self.position_history.append(self.current_point)

    def run_turn(self):
        pass


class BoardPlayerRandomMoviment(BoardPlayer):

    # ogni 3 secondi effettua un movimento
    MOVIMENT_TIMER = 2

    def __init__(self,  board_service, x, y):
        super().__init__(board_service, x, y)
        self._moviment_counter = BoardPlayerRandomMoviment.MOVIMENT_TIMER

    def decrease_moviment_counters(self):
        if self._moviment_counter > 0:
            self._moviment_counter = self._moviment_counter - 1
        else:
            self._moviment_counter = BoardPlayerRandomMoviment.MOVIMENT_TIMER

    def run_turn(self):
        self.actions = [a for a in self.actions if not a.executed]
        if self.actions:
            for a in self.actions:
                a.emit()
        elif self._moviment_counter == 0:
            self.init = False
            action =  PlayerActionMove(self, self.board_service)
            k = random.randrange(0, len(BoardService.POSITIONS))
            x, y = BoardService.POSITIONS[k]
            action.set_payload({"x": x, "y": y, "target": self})
            self.actions.append(action)
            action.emit()
            self._moviment_counter = BoardPlayerRandomMoviment.MOVIMENT_TIMER
        if not self.actions:
            self.decrease_moviment_counters()


def test_player():
    board_service = BoardService()
    player_1 = BoardPlayerRandomMoviment(board_service, 0, 0)
    board_service.add_player(player_1)
    board_service.run()


if __name__ == "__main__":
    test_player()

